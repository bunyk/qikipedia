const DEBUG = false;
const MAX_WORD_LIMIT = 7;
const CARD_ID = 'qikipedia';
const CARD_WIDTH = 350;
const CARD_PADDING = 14;

let mouseX;
let mouseY;
let cardShowing = false;

/**
 * Mouse up event
 * @param e
 */
function mouseUp(e) {
    debug(options);
    if (options.enabled) {
        if (e.target.tagName === 'INPUT' || e.target.tagName === 'TEXTAREA') {
            return;
        }
        debug(e.target.tagName);

        if (cardShowing) {
            const card = e.path.find(elem => {
                return elem.id === CARD_ID;
            });
            if (!card) {
                removeCard();
            }
        } else {
            removeCard();
            const selectedText = getSelectionText();
            if (selectedText) {
                const selectedWords = selectedText.split(" ").map(word => {
                    word = word.replace(/[&\/\\#,+();_\-$~%.’'":*?<>{}]/g, '');
                    if (word.length > 0) {
                        return word;
                    }
                });
                debug(selectedWords);
                if (selectedWords.length > 0 && selectedWords.length < MAX_WORD_LIMIT) {
                    fetchWikiSummary(selectedWords);
                }
            }
        }
    }
}

/**
 * Mouse down event
 * @param e
 */
function mouseDown(e) {
    if (options.enabled) {
        mouseX = e.pageX;
        mouseY = e.pageY;
    }
}

/**
 * Tries to fetch Wikipedia summary, if available
 * @param words
 */
function fetchWikiSummary(words) {
    if (words) {
        const url = `https://${options.lang}.wikipedia.org/api/rest_v1/page/summary/${words.join("_")}`;
        debug(url);
        fetch(url)
            .then(response => response.json())
            .then(json => {
                debug(json);
                const text = json.extract_html;
                if ((json.type === 'standard' || json.type === 'disambiguation') && text) {
                    const image = json.thumbnail ? json.thumbnail.source : null;
                    const articleUrl = json.content_urls ? json.content_urls.desktop.page : null;
                    if (text && text.split(" ").length > 6) { // Avoids showing empty disambiguation responses
                        addCard(image, text, articleUrl);
                    }
                }
            }).catch(err => {
            debug(err);
        });
    }
}

/**
 * Returns currently selected text
 * @returns {*}
 */
function getSelectionText() {
    let text = null;
    if (window.getSelection) {
        text = window.getSelection().toString();
    } else if (document.selection && document.selection.type !== "Control") {
        text = document.selection.createRange().text;
    }
    return text;
}

/**
 * Renders card on screen with given info
 * @param image
 * @param body
 * @param articleUrl
 */
function addCard(image, body, articleUrl) {
    removeCard();
    const data = {
        image: image,
        imageHeight: image ? 150 : 0,
        body: body,
        articleUrl: articleUrl,
        theme: options.dark ? 'dark' : 'light',
    };
    const popup = Mustache.render(TEMPLATE, data);
    const popupNode = document.createElement('div');

    let x;
    if (mouseX + CARD_WIDTH / 2 > window.innerWidth) {
        x = window.innerWidth - CARD_WIDTH - CARD_PADDING * 2;
    } else if (mouseX - CARD_WIDTH / 2 < 0) {
        x = CARD_PADDING;
    } else {
        x = mouseX - CARD_WIDTH / 2;
    }

    popupNode.style.left = `${x}px`;
    popupNode.style.top = `${mouseY + CARD_PADDING}px`;
    popupNode.setAttribute("id", CARD_ID);
    popupNode.innerHTML = popup;
    document.body.appendChild(popupNode);
    cardShowing = true;
    setTimeout(function () {
        popupNode.setAttribute("class", CARD_ID + '-show');
    }, 10);
}

/**
 * Removes card from screen
 */
function removeCard() {
    const card = document.getElementById(CARD_ID);
    if (card) {
        card.setAttribute("class", '');
        setTimeout(function () {
            if (card.parentElement) {
                document.body.removeChild(card);
            }
            cardShowing = false;
        }, 500);
    }
}

function debug(msg) {
    if (DEBUG) {
        console.log(msg);
    }
}

/**
 * Keep polling until DOM is ready
 */
chrome.extension.sendMessage({}, response => {
    let readyStateCheckInterval = setInterval(function () {
        if (document.readyState === "complete") {
            clearInterval(readyStateCheckInterval);

            // Page loaded
            restoreOptions(function () {
                document.addEventListener("mousedown", mouseDown, false);
                document.addEventListener("mouseup", mouseUp, false);
            });
        }
    }, 10);
});