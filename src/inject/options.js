// Default options
let options = {
    enabled: true,
    dark: false,
    lang: "en"
};

/**
 * Saves options to chrome.storage
 */
function saveOptions(newOptions, callback) {
    options.enabled = newOptions.hasOwnProperty('enabled') ? newOptions.enabled : options.enabled;
    options.dark = newOptions.hasOwnProperty('dark') ? newOptions.dark : options.dark;
    options.lang = newOptions.hasOwnProperty('lang') ? newOptions.lang : options.lang;

    chrome.storage.sync.set({
        options: options
    }, callback);
}

/**
 * Restores select box and checkbox state using the preferences
 * stored in chrome.storage.
 */
function restoreOptions(callback) {
    chrome.storage.sync.get({
        options: options
    }, function (items) {
        options.enabled = items.options.enabled;
        options.dark = items.options.dark;
        options.lang = items.options.lang;

        if (callback) {
            callback();
        }
    });
}

/**
 * Update extension icon to reflect the state
 */
function updateUI() {
    if (chrome.browserAction) {
        let iconName = options.enabled ? '48' : '-off';
        let title = options.enabled ? chrome.i18n.getMessage("enabled") : chrome.i18n.getMessage("disabled");
        chrome.browserAction.setIcon({path: "icons/icon" + iconName + ".png"});
        chrome.browserAction.setTitle({title: title});
    }
}

chrome.storage.onChanged.addListener((changes, namespace) => {
    options = changes.options.newValue;
    updateUI();
});