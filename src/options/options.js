/**
 * Saves options to chrome.storage
 */
function save() {
    options.enabled = document.getElementById('enabled').checked;
    options.dark = document.getElementById('dark').checked;
    options.lang = document.getElementById('lang').value;

    saveOptions(options, function () {
        // Update status to let user know options were saved.
        let status = document.getElementById('status');
        status.textContent = 'Settings saved.';
        setTimeout(function () {
            status.textContent = '';
        }, 1500);
    });
}

/**
 * Called when settings screen has loaded
 */
function onLoad() {
    // Set version number from manifest
    const manifestData = chrome.runtime.getManifest();
    document.getElementById("version").innerHTML = "Version " + manifestData.version;

    // Set default values.
    restoreOptions(function () {
        document.getElementById('enabled').checked = options.enabled;
        document.getElementById('dark').checked = options.dark;
        document.getElementById('lang').value = options.lang;
    });
}

document.addEventListener('DOMContentLoaded', onLoad);
document.getElementById('save').addEventListener('click', save);